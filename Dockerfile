FROM debian:10 as build

RUN apt update && apt install -y wget gcc tree make libpcre3 libpcre3-dev zlib1g-dev openssl libssl-dev
RUN wget https://nginx.org/download/nginx-1.20.1.tar.gz && \
    tar xvfz nginx-1.20.1.tar.gz && \ 
    cd nginx-1.20.1 && \ 
    ./configure \
	--sbin-path=/usr/bin/nginx \
	--conf-path=/etc/nginx/nginx.conf \
	--error-log-path=/var/log/nginx/error.log \
	--http-log-path=/var/log/nginx/access.log \
	--with-pcre && \
    
    make && \ 
    make install

FROM debian:10
WORKDIR /usr/bin
COPY --from=build /usr/bin/nginx .
RUN mkdir /etc/nginx /var/log/nginx /usr/local/nginx && \ 
chmod +x nginx && \
touch /var/log/error.log && \
touch /var/log/access.log
COPY --from=build /etc/nginx/* /etc/nginx/
COPY --from=build /usr/local/nginx/ /usr/local/nginx/ 
CMD ["nginx", "-g", "daemon off;"]
